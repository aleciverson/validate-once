BIN  := serve-wasm
WASM := static/validate.wasm

$(BIN): go.mod main.go validate/validate.go | $(WASM)
	go build -o $@

$(WASM): validate_js/main.go go.mod validate/validate.go
	GOOS=js GOARCH=wasm go build -o $@ ./$(<D)

.PHONY: clean
clean:
	@-rm -f $(BIN)
	@-rm -f $(WASM)
