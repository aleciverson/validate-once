# Validate "Once"

## What

The same validation code is used on both the frontend _and_ the backend!

## Build

run `make`

## Try it

run `./serve-wasm`
