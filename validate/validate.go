package validate

import (
	"errors"
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

// Validate checks if the given string meets the following criteria:
//
// * Is valid UTF-8 text
// * Has a length of 8 characters
// * Has at least 1 letter character
// * Has at least 1 digit character
// * Has at least 1 special character
// * Has no whitespace characters
//
// If any of the criteria is not met
func Validate(s string) error {
	var errs errorList

	if !utf8.ValidString(s) {
		errs = append(errs, errors.New("must be valid UTF-8 text"))
	}

	if len(s) != 8 {
		errs = append(errs, fmt.Errorf("must be 8 characters, is %d", len(s)))
	}

	var (
		hasAlpha      = false
		hasDigit      = false
		hasSpecial    = false
		hasWhitespace = false
	)

	for _, c := range s {
		switch {
		case unicode.IsLetter(c):
			hasAlpha = true
		case unicode.IsDigit(c):
			hasDigit = true
		case unicode.IsSymbol(c) || unicode.IsPunct(c):
			hasSpecial = true
		case unicode.IsSpace(c):
			hasWhitespace = true
		}
	}

	if !hasAlpha {
		errs = append(errs, errors.New("must contain at least 1 alpha character"))
	}

	if !hasDigit {
		errs = append(errs, errors.New("must contain at least 1 digit character"))
	}

	if !hasSpecial {
		errs = append(errs, errors.New("must contain at least 1 special character"))
	}

	if hasWhitespace {
		errs = append(errs, errors.New("must not contain any whitespace"))
	}

	return errs
}

type errorList []error

func (l errorList) Error() string {
	if len(l) != 0 {
		var sb strings.Builder

		sb.WriteString(l[0].Error())
		for _, e := range l[1:] {
			sb.WriteByte('\n')
			sb.WriteString(e.Error())
		}

		return sb.String()
	}
	return ""
}
