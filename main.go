// +build !js,!wasm

package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"dabbertorres.dev/serve-wasm/validate"
)

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/submit", func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, err.Error())
			return
		}

		if err := validate.Validate(string(body)); err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
			fmt.Fprint(w, err.Error())
			return
		}

		if _, err := w.Write(bytes.ToUpper(body)); err != nil {
			log.Println("error responding:", err)
		}
	})

	mux.Handle("/", http.FileServer(http.Dir("./static")))

	srv := &http.Server{
		Addr:              ":8080",
		Handler:           mux,
		ReadHeaderTimeout: 5 * time.Second,
		IdleTimeout:       30 * time.Minute,
		MaxHeaderBytes:    1 << 14,
	}

	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal(err)
	}
}
