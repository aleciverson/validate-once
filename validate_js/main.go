// +build js,wasm

package main

import (
	"errors"
	"os"
	"syscall/js"

	"dabbertorres.dev/serve-wasm/validate"
)

func main() {
	ch := make(chan bool)
	f := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		if len(args) == 0 {
			return errors.New("must provide an argument").Error()
		}

		arg := args[0]

		if arg.Type() != js.TypeString {
			return errors.New("must be text").Error()
		}

		if err := validate.Validate(arg.String()); err != nil {
			return err.Error()
		}
		return js.Null()
	})
	defer f.Release()

	funcName := "validate"
	if len(os.Args) > 1 {
		funcName = os.Args[1]
	}

	js.Global().Set(funcName, f)

	<-ch
}
